$(function(){
    $("#parse").click(function(){
        $("#text_loader").removeClass('hide').show();
        $("#title_loader").removeClass('hide').show();
        $('#text').hide();
        $('#title').hide();
        $.ajax({
            type: "GET",
            url: "/api/v1/parse",
            data: {url : $('#address').val()},
            datatype: "json",
            success: function(data){
                $('#text').text(data.text).show();
                $('#title').text(data.title).show();
                $("#text_loader").hide();
                $("#title_loader").hide();
            }
        })
    })
});