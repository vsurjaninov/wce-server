package ru.vsurjaninov.wceserver;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.log.Log;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.servlet.ServletContainer;

public class ServerRunner {
    public static void main(String[] args){

        Server server = new Server(8080);
        ResourceHandler resourceHandler = new ResourceHandler();
        resourceHandler.setDirectoriesListed(true);
        resourceHandler.setResourceBase("./web/");
        resourceHandler.setWelcomeFiles(new String[]{"index.html"});

        ServletContextHandler jerseyHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        jerseyHandler.setContextPath("/api/v1");
        ServletHolder servletHolder = new ServletHolder(new ServletContainer());
        servletHolder.setInitParameter(ServerProperties.PROVIDER_PACKAGES, "ru.vsurjaninov.wceserver.rest");
        servletHolder.setInitOrder(1);
        jerseyHandler.addServlet(servletHolder, "/*");

        HandlerList handlerList = new HandlerList();
        handlerList.addHandler(jerseyHandler);
        handlerList.addHandler(resourceHandler);

        server.setHandler(handlerList);

        try {
            server.start();
        } catch (Exception e) {
            Log.getLog().debug(e);
        }

        Log.getLog().info("Open \"localhost:8080\" in your browser");
    }
}
