package ru.vsurjaninov.wceserver.rest;

import org.eclipse.jetty.util.log.Log;
import ru.vsurjaninov.webslug.Extractor;
import ru.vsurjaninov.webslug.processing.Content;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("/")
public class WCEResource {

    final Extractor extractor = Extractor.getDefaultInstance();

    @GET
    @Path("/parse")
    @Produces(MediaType.APPLICATION_JSON)
    public Response parse( @QueryParam("url") String url ) {

        Content content = new Content();
        try{
            content = extractor.extractContent(url);
        } catch (IllegalArgumentException|IOException e){
            Log.getLog().warn("Failed to extract text from url: " + url);
            Log.getLog().warn("Cause: " + e.getMessage());
        }

        return Response.ok(content).build();
    }
}
